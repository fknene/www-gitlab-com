module Gitlab
  module Homepage
    class Group
      LABEL_PREFIX = 'group::'.freeze

      attr_reader :key

      def initialize(key, data)
        @key = key
        @data = data
      end

      # rubocop:disable Style/MethodMissingSuper
      # rubocop:disable Style/MissingRespondToMissing

      ##
      # Middeman Data File objects compatibility
      #
      def method_missing(name, *args, &block)
        @data[name.to_s]
      end

      def label
        "#{LABEL_PREFIX}#{key.tr('_', ' ')}"
      end

      # rubocop:enable Style/MethodMissingSuper
      # rubocop:enable Style/MissingRespondToMissing

      def self.all!
        @all ||= YAML.load_file(File.expand_path('../data/stages.yml', __dir__))['stages'].flat_map do |stage_key, stage_data|
          stage_data['groups'].map do |group_key, group_data|
            group_data['section'] = stage_data['section']
            new(group_key, group_data)
          end
        end
      end
    end
  end
end
