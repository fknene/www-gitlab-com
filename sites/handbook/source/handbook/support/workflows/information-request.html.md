---
layout: handbook-page-toc
title: Subpoenas, Court Orders and other requests for user information
category: Legal
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview

From time to time we may receive subpoenas or other legal requests for information about GitLab users, their data or activity.
This workflow clarifies how to handle these requests and subsequent workflow related to delivering information if
our counsel and CISO approve.

## Workflow

### Incoming Subpoena or court order

*Note:* If a ticket comes in that you believe is Legal related, but is not a subpoena or court order, please check the list of other [legal workflows](https://about.gitlab.com/handbook/support/workflows/#Legal).

If a subpoena or court order that is not covered by another workflow (e.g. DMCA, GDPR, information request) comes in:

1. Forward the email to legal@gitlab.com
1. Reach out on the Slack #legal channel to inform the Legal team.
1. Respond to the ticket:

```
Your request has been received and forwarded to our legal department who will review this request.

Please direct any follow-up or additional queries of this nature directly to the legal team by using the email address legal@gitlab.com.

This ticket will be marked as "Solved"
```

### Unsure? Other?

Support should be handling responses to any inquiries for clarifications. In most cases, an existing customer will be referred to their account manager.

If you're unsure on how to respond, post in the #support_managers Slack channel for guidance.
